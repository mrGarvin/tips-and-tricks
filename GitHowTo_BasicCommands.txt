First time:
1. Set your identity globaly:
	git config --global user.name "John Doe"
	git config --global user.email johndoe@example.com

------------------------------------------------------------

New Git repository:
1. Open Git Bash

2. Navigate to the directory you were want create a Git repository:
	cd "C:\PATH\my-solution-directory"
(See naming-TIP below)

3. Create an empty Git repository:
	git init

(This is optional)
4. Add .gitignore (Highly recommended):
	Touch .gitignore
(This will create an empty file in your solution directory named '.gitignore'. Open the file with a text editor and add which files and directories that should be ignored. A default pattern for Visual Studio is located at the end of this document.)

(This is optional. If you don't do this step all commits, history, etc will only be saved locally)
5. Set which repository to push the files too (Ideally the repository name and solution name should be the same, see naming-TIP below):
	git remote add origin "https://jDoe@bitbucket.org/jDoe/my-solution-directory.git"
	If that don't work use:
		git remote add origin "https://bitbucket.org/jDoe/my-solution-directory.git"

6. Add files and directories to index:
	git add .							(All files)
	git add nameOfFileOrDirectory		(Single file or directory)

7. Commit with message:
	git commit -m "Message" -a

8. Push files:
	git push --set-upstream origin master
	If that don't work use:
		git push -u origin master

If you forgot to add a .gitignore before first commit:
Commit any changes. Create a .gitignore as described above and run the following commands:
	git rm -r �-cached .
	git add .
	git commit -m "Fixed untracked files."

TIP: (If you use Camel case when naming your solution) Place your solution in a folder with the same name but use hyphen to separate the words instead. The reason is, if you push to BitBucket or a similar web-hosting service, in the link to the repository the name will be in lowercase even if you name it with uppercases. You can change the name in the link but you will probably get tired of that pretty quick.
Ex.:
(Recommended)
my-solution
	.git
	mySolution
		.vs
		mySolution
(Not recommended)
mySolution
	.git
	.vs
	mySolution

------------------------------------------------------------

Update repository:
1. Open Git Bash

2. Navigate to the directory where your Git repository is located:
	cd "C:\PATH\my-solution-directory"

3. Add files and directories to index:
	git add .				(All files)
	git add nameOfFileOrDirectory		(Single file or directory)

4. Commit with message:
	git commit -m "message"

5. Push files:
	git push

------------------------------------------------------------

Get a copy of an existing Git repository (Clone Git repository):
1. Open Git Bash

2. Navigate to the directory you were want create a cloned Git repository:
	cd "C:\PATH

3. Clone the repository:
	git clone "https://jDoe@bitbucket.org/jDoe/my-solution-directory.git"
	if that don't work use:
		git clone "https://bitbucket.org/jDoe/my-solution-directory.git"

------------------------------------------------------------

Get updated repository:
1. Open Git Bash

2. Navigate to the directory where your Git repository is located:
	cd "C:\PATH\my-solution-directory"

3. "Download" the updated repository:
	git pull origin master

------------------------------------------------------------

"Make an other branch master":
(NOTE: This might NOT be a good idea if you going to push to a public repository!)
git checkout better_branch
git merge --strategy=ours master    # keep the content of this branch, but record a merge
git checkout master
git merge better_branch             # fast-forward master up to the merge

If you want your history to be a little clearer, I'd recommend adding some information to the merge commit message to make it clear what you've done. Change the second line to:

git merge --strategy=ours --no-commit master
git commit          # add information to the template merge message

(From StackOverflow: http://stackoverflow.com/questions/2763006/change-the-current-branch-to-master-in-git)

------------------------------------------------------------

See all branches:
	git branch

------------------------------------------------------------

Create a new local branch:
	git checkout -b name_of_your_new_branch

------------------------------------------------------------

'Merge' specific files from an other branch:
(If you are a coward, create a new branch to test the 'merge' in.)
1. Checkout files from branch:
	git checkout name_of_branch_with_desired_files Directory1/Directory1.1/File1.fileType Directory1/Directory1.2/File2.fileType Directory2/*
2. Check status:
	git status
3. Commit with message:
	git commit -m "'Merged' files from 'name_of_branch_with_desired_files' branch"

(NOTE: Write "git checkout ..." on one line. "*" will add all files in a directory)
(From: http://jasonrudolph.com/blog/2009/02/25/git-tip-how-to-merge-specific-files-from-another-branch/)

------------------------------------------------------------


#Default ignores from Git Extensions
#ignore thumbnails created by windows
Thumbs.db
#Ignore files build by Visual Studio
*.obj
*.exe
*.pdb
*.user
*.aps
*.pch
*.vspscc
*_i.c
*_p.c
*.ncb
*.suo
*.tlb
*.tlh
*.bak
*.cache
*.ilk
*.log
[Bb]in
[Dd]ebug*/
*.lib
*.sbr
obj/
[Rr]elease*/
_ReSharper*/
[Tt]est[Rr]esult*
